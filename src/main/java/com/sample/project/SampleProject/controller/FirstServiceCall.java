package com.sample.project.SampleProject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sample.project.SampleProject.service.RabbitMQSender;
import com.sample.project.SampleProject.service.TestClass;

@RestController
public class FirstServiceCall {
	
	@Autowired
	TestClass testClass;
	
	@Autowired
	RabbitMQSender rabbitMQSender;
	
	@GetMapping("/test")
	public String firstCall () {
		testClass.testSecondJars();
		
		return "hello";
	}
	

	/*
	 * @GetMapping("/producer") public String producer() {
	 * System.out.println("hello"); String rabbitTest = "hello rabbit mq test";
	 * rabbitMQSender.send(rabbitTest);
	 * 
	 * return "Message sent to the RabbitMQ JavaInUse Successfully"; }
	 */

}
