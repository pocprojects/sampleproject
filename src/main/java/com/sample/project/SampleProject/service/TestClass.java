package com.sample.project.SampleProject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sample.project.SampleProject.methods.SampleMethods;

@Service
public class TestClass {
	
	@Autowired
	SampleMethods methods;
	
	public void testSecondJars() {
		
		methods.getData();
	}

}
