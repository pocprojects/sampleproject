package com.sample.project.SampleProject.service;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;



@Service
public class RabbitMQListner {
	
	/*
	 * @Autowired private AmqpTemplate amqpTemplate;
	 * 
	 * @Value("${javainuse.rabbitmq.exchange}") private String exchange;
	 * 
	 * @Value("${javainuse.rabbitmq.routingkey}") private String routingkey; String
	 * kafkaTopic = "java_in_use_topic";
	 * 
	 * public void send(String company) { amqpTemplate.convertAndSend(exchange,
	 * routingkey, company); System.out.println("Send msg = " + company);
	 * 
	 * }
	 */
	
	@RabbitListener(queues="${javainuse.rabbitmq.queue}")
    public void receivedMessage(String msg) {
        System.out.println("Received Message: " + msg);
    }
}