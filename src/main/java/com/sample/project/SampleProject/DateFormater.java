package com.sample.project.SampleProject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.TimeZone;

import com.fasterxml.jackson.databind.util.ISO8601DateFormat;

public class DateFormater {

	public static void main(String[] args) {
		
		
		// TODO Auto-generated method stub
		
		DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		String string1 = "2001-07-04T12:08:56.235-0700";
		try {
			Date result1 = df1.parse(string1);
			System.out.println(result1);
			
			ISO8601DateFormat df2 = new ISO8601DateFormat();
			Date d = df2.parse("2019-08-13T08:47:41.519Z");
			System.out.println(d.getTime());
			
			java.util.Date date = Date.from(Instant.parse( "2019-08-13T08:47:41.519Z" ));//TODO: We can use this
			
			System.out.println("----"+date.getTime());
			
			
			
			TimeZone tz = TimeZone.getTimeZone("UTC");
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			df.setTimeZone(tz);
			String nowAsISO = df.format(new Date());

			System.out.println(nowAsISO);

			DateFormat df3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
			//nowAsISO = "2013-05-31T00:00:00Z";
			Date finalResult = df3.parse(nowAsISO);

			System.out.println(finalResult);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
