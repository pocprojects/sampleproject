package com.sample.project.SampleProject;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CompletableFutureTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("Inside : " + Thread.currentThread().getName());
		
		ExecutorService executor = Executors.newFixedThreadPool(5);
		CompletableFuture<String> cf = CompletableFuture.supplyAsync(()->{
			
			System.out.println("Inside : " + Thread.currentThread().getName());
		    return "Hello";
		},executor);//thenApplyAsync(value-> value.toUpperCase(), executor);
		
		try {
			
			System.out.println("Value- " + cf.get());
		
			
			if (cf.get() != null) {
				cf.thenApplyAsync(value -> {
					//value.toUpperCase();
					System.out.println("value 111" + value.toUpperCase());
					System.out.println("Inside : " + Thread.currentThread().getName());
					return value.toUpperCase();
				}, executor).thenApplyAsync(nextValue ->

				{
					
					System.out.println("new hello" + nextValue.replace(nextValue, "new hello"));
					System.out.println("Inside : " + Thread.currentThread().getName());
					return nextValue.replace(nextValue, "new hello");

				}, executor).thenAcceptAsync(finalValue ->

				{
					System.out.println("final value" + finalValue);
					System.out.println("Inside : " + Thread.currentThread().getName());

				});
			}
			 
		
			// Using Lambda Expression
			CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
			    // Simulate a long-running Job   
			    try {
			    	System.out.println("Inside : " + Thread.currentThread().getName());
			        TimeUnit.SECONDS.sleep(1);
			    } catch (InterruptedException e) {
			        throw new IllegalStateException(e);
			    }
			    System.out.println("I'll run in a separate thread than the main thread.");
			},executor);
			
			// Block and wait for the future to complete
			future.get();
			
			// Using Lambda Expression
			CompletableFuture<String> future1 = CompletableFuture.supplyAsync(() -> {
			    try {
			    	System.out.println("Inside : " + Thread.currentThread().getName());
			        TimeUnit.SECONDS.sleep(5);
			    } catch (InterruptedException e) {
			        throw new IllegalStateException(e);
			    }
			    return "Result of the asynchronous computation";
			},executor);
			
			System.out.println(future1.get());
			
			CompletableFuture<String> welcomeText = CompletableFuture.supplyAsync(() -> {
			    try {
			        TimeUnit.SECONDS.sleep(1);
			    } catch (InterruptedException e) {
			       throw new IllegalStateException(e);
			    }
			    return "Rajeev";
			}).thenApply(name -> {
			    return "Hello " + name;
			}).thenApply(greeting -> {
			    return greeting + ", Welcome to the CalliCoder Blog";
			});

			System.out.println(welcomeText.get());
			// Prints - Hello Rajeev, Welcome to the CalliCoder Blog
			
			

		
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		executor.shutdown();
		System.out.println("Inside : " + Thread.currentThread().getName());

	}

}
