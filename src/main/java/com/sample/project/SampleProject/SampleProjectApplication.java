package com.sample.project.SampleProject;

import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleProjectApplication {

	public static void main(String[] args) {
		Map<String, String> env = System.getenv();
	    for (String key : env.keySet())
	    {
	        System.out.println(key + ":" + env.get(key));
	    }

	    System.out.println(System.getenv("KYMA_STREAMER_ADDRESS"));
		SpringApplication.run(SampleProjectApplication.class, args);
	}

}
